# ZoomInfo Python

**TLDR**: 
- A script to query/test the ZoomInfo rest api. 

## Description

The script hits various zoominfo endpoints with various query parameters.

- A config file defines the urls and query parameters.

- You can add/change queries without touching the python code--just add entries to the yml file.

- Thus you can "poke around" with various endpoints and parameters without touching the python code

## How to Use

- Set up your python


    python install -r requirements.txt

- Ask around for the password 

- Tweak the conf/config.yml as needed
- Run as follows:


    main.py --password=<<PASSWORD>> --conf=config/config.yml


Note:  you can put the password in the config.yml file rather than pass it on the command line.

To do so: Uncomment the 'password' entry and put a valid value
```
    login: b.milbratz@onriva.com
#    password: USE_CMDLINE
```


## Advanced

### Adding queries

To add a new query, simply add to the "zoominfo_urls" list in the config file.


```
zoominfo_urls:
  - path: '/search/contact'
    limit: 5
    query:
      lastName: milbratz

  - path: '/search/company'
    query:
      companyName: onriva

```

## Reference
ZoomInfo API is documented here: [ZoomInfo Enterprise Search] (https://api-docs.zoominfo.com/#fc31163c-ad00-46f9-be2e-42f4193b890b)


## Questions

slack me:  b.milbratz@onriva.com