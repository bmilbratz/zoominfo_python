#!/usr/bin/env python
"""
Script to get data from zoominfo api
"""

import argparse
import datetime
import json
import logging
import urllib
from timeit import default_timer as timer
from urllib import parse

import requests
import urllib3
import yaml


class ZoomInfoService:

    def __init__(self):
        self.token = ""
        self.url_base = ""
        self.dry_run = False
        self.max_count = 0
        self.default_page_size = 50
        self.out_dir = ""
        self.env_name = ""
        self.user = ""
        self.password = ""

    def do_login(self):
        """
        Performs login
        :return:
        """

        url = f"{self.url_base}/authenticate"
        logging.debug(f"{self.user} password {self.password}")
        logging.info(f"logging into url {url}")
        payload = {'username': self.user, 'password': self.password}
        headers = {'Content-Type': 'application/json'}

        logging.info(f"url is {url} account is {self.user}")

        # Do not log in if dry run
        if self.dry_run:
            logging.info("dry run : not logging in")
            return 1

        r = requests.post(url, data=json.dumps(payload), headers=headers, verify=False)
        if r.status_code == 200:
            bg_return_val = 1
            r_json = r.json()
            self.token = r_json.get('jwt')
            logging.info(f"OK Login success {r.status_code} token:{self.token}")
            logging.info(f"login response headers are f{r.headers}")
            logging.info(f"login response body is: {r.text}")
        else:
            self.token = ""
            logging.error("Error logging in. Status Code: " + str(r.status_code))
            bg_return_val = -1

        return bg_return_val

    def do_logout(self):
        """
        Do logout
        :return:
        """

        if self.dry_run:
            logging.info(f"dry run: not logging out token was: {self.token}")
            return

        # logout is no-op w/ zoominfo
        return

    def do_get_zoominfo_data(self, zoominfo_urls):

        return_val = self.do_login()
        if return_val != 1:
            return

        uber_entries = []
        for item in zoominfo_urls:
            try:
                # login
                #return_val = self.do_login()
                #if return_val == 1:
                    start_time = timer()
                    path = item.get("path")
                    query_dict = item.get("query")
                    if not query_dict:
                        query_dict = {}

                    logging.info(f"Process path: {path}")
                    logging.info(f"query: {query_dict}")

                    pagesize_parm_name = 'rpp'
                    if query_dict.get(pagesize_parm_name) is None:
                        query_dict[pagesize_parm_name] = self.default_page_size
                    page_size = int(query_dict.get(pagesize_parm_name))

                    cur_page = 0
                    total = 0
                    b_done = False
                    while not b_done:
                        logging.info(f"cur_page = {cur_page}")

                        query_dict["page"] = cur_page
                        encoded_path = urllib.parse.quote(path)
                        status_code, entries = self.do_pull_data(encoded_path, query_dict)

                        if not entries or len(entries) < page_size:
                            logging.info("done ")
                            b_done = True

                        cur_page += 1

                        # if cur_page * page_size > item.get("limit"):
                        #     b_done = True

                        if entries:
                            total += len(entries)
                            entry_dict = {
                                "path": path,
                                "query string": query_dict,
                                "total": total,
                                "cur_page": cur_page,
                                "entries": entries
                            }
                            uber_entries.append(entry_dict)

                        end_time = timer()
                        logging.info(f"for path:{path} total:{total} took: {round(end_time - start_time, 2)}")

                        # break if past max
                        if self.max_count and total >= self.max_count:
                            b_done = True

            except Exception as inst:
                logging.exception(f"Exception pulling data: {inst}")
            finally:
                logging.debug("cleaning up")
                # Always logout
                # self.do_logout()

        # Always logout
        self.do_logout()

        fmt = '%Y_%m%d_%H%M'
        cur_time = datetime.datetime.now()
        out_path = f"{self.out_dir}/output_{self.env_name}_{cur_time.strftime(fmt)}.json"
        with open(out_path, 'w') as outfile:
            logging.info(f"dumping file to {out_path} env is {self.env_name} ")
            json.dump(uber_entries, outfile, indent=3)

    # Pull all data back from form
    def do_pull_data(self, path, query_dict):
        """
        Fetch data from server
        :param path:
        :param query_dict:
        :return: size of entry
        """
        bg_token_type = 'Bearer'

        path_url = f"{self.url_base}{path}"
        # form_url = f"{path_url}?{query_string}"
        # logging.info(f"form url {form_url}")

        auth_headers = {'Authorization': bg_token_type + ' ' + self.token}

        if self.dry_run:
            # logging.info (f"dry run: not pulling data")
            return 200, None

        # Pull back all data in the form
        start_time = timer()
        response = requests.post(url=path_url, headers=auth_headers, verify=False, json=query_dict)
        end_time = timer()

        logging.debug(f"pull data request headers {response.request.headers} response headers are f{response.headers}")

        # URL Used: {response.url}
        #  f"headers are  {response.headers}")

        if response.status_code != 200:
            logging.info(f'Status code: {response.status_code}')
            logging.info(f'Headers: {response.headers}')
            logging.info(f'Error Response:  {response.text}')
            return -1, None

        bg_data = response.json()
        # originally this code only called stuff that
        # wrapped collections w/ attribute 'data'
        # ...then we started calling more general apis
        # ..this is a hack to "scratch both itches"
        if isinstance(bg_data,dict) and bg_data.get('data'):
            entries = bg_data.get('data')
        else:
            entries=bg_data

        seconds = round(end_time - start_time, 2)
        logging.info(f"Status code: {response.status_code} nbr entries:{len(entries)} seconds:{seconds} ")
        logging.info(f"Got response body : {bg_data}")

        return response.status_code, entries


def main():
    """
    Main
    :return:
    """
    # Login and get token

    parser = argparse.ArgumentParser(
        description='python rest api script'
    )
    parser.add_argument('--user',
                        help='user',
                        dest='user', required=False)
    parser.add_argument('--password', dest='password', help='password', required=False)
    parser.add_argument('--conf',
                        help='conf',
                        dest='conf', required=True)

    args = parser.parse_args()

    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    config_file = args.conf
    with open(config_file, 'r') as ymlfile:
        yml_load = yaml.load(ymlfile, Loader=yaml.FullLoader)

        app_cfg = yml_load['get_urls']
        environment = app_cfg.get("environment")
        env_config = yml_load["env_config"]
        env_web_config = env_config[environment]
        logging_cfg = yml_load["logging"]
        zoominfo_urls = yml_load["zoominfo_urls"]

    logging.basicConfig(level=logging_cfg["level"],
                        format='%(asctime)s [%(levelname)s] %(message)s')

    url = env_web_config['url']

    if args.password:
        password = args.password
    else:
        password = env_web_config['password']

    if args.user:
        user = args.user
    else:
        user = env_web_config['login']

    zoominfo_service = ZoomInfoService()
    zoominfo_service.url_base = url
    zoominfo_service.dry_run = app_cfg.get("dry_run")
    zoominfo_service.max_count = int(app_cfg.get("max_count", 0))
    zoominfo_service.out_dir = app_cfg.get("out_dir")
    zoominfo_service.env_name = app_cfg.get("environment")
    zoominfo_service.user = user
    zoominfo_service.password = password

    zoominfo_service.do_get_zoominfo_data(zoominfo_urls)


main()
